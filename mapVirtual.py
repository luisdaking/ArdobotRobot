from scipy import ndimage
import numpy as np
import matplotlib.pyplot as plt
from skimage import draw
import json
import math


class MapRobot:
    # ancho
    w = 0
    # alto
    h = 0
    # terreno
    ground = 0

    _color_yellow = [255, 255, 0]
    _color_red = [255, 0, 0]
    _color_indigo = [145, 0, 255]


    def __init__(self):
        self.config = json.load(open("config.json", "r"))

        self.w = self.config.get("map").get("width")
        self.h = self.config.get("map").get("higth")
        self.ground = np.zeros((self.h, self.w, 3), dtype=np.uint8)

        self.startMap()


    def startMap(self):

        self.posicionInicialMapa()

        self.ubicarBarreras(3, 30)

        self.ubicarFlamas(3, 15)



    def posicionInicialMapa(self):
        """

        :return:
        """
        # posicion inicial
        rr, cc = self.car = draw.circle(
            self.w / 2,
            self.h / 2,
            10
        )

        self.ground[rr, cc] = self._color_yellow

    def ubicarBarreras(self, cantidad, longitud):
        """
            Ubica barreras
        :param cantidad:
        :return:
        """

        for i in range(cantidad):

            y0 = np.random.randint(self.w)
            x0 = np.random.randint(self.h)

            y1 = np.random.randint(self.w)
            x1 = np.random.randint(self.h)

            alpha = math.atan((x1-x0) / (y1-y0))
            y1 = int(math.sin(alpha) * longitud) + y0
            x1 = int(math.cos(alpha) * longitud) + x0

            rr, cc, val = draw.line_aa(y0, x0, y1, x1)

            self.ground[rr, cc] = self._color_red

    def ubicarFlamas(self, cantidad, radio):
        """

        :param cantidad:
        :param longitud:
        :return:
        """

        for i in range(cantidad):
            w2 = self.w - 2 * radio
            h2 = self.h - 2 * radio

            x = np.random.randint(w2)
            y = np.random.randint(h2)

            # perimetro externo
            rr, cc, val = draw.circle_perimeter_aa(x, y, radio)
            self.ground[rr, cc] = self._color_indigo

            # flama
            rr, cc = draw.circle(x, y, int(radio/4))
            self.ground[rr, cc] = self._color_yellow




    def distance(self, y0, x0, y1, x1):
        return math.sqrt(
            math.pow(y1-y0, 2) +
            math.pow(x1-x0, 2)
        )






    def showMap(self):
        plt.imshow(self.ground)
        plt.show()






if __name__ == "__main__":

    # crear objeto
    m = MapRobot()
    m.showMap()

